# Declarate

This project implements a dialect of the DECORATE, a declarative actor definition language from ZDoom. This serves as a proof of concept code generator for Chocolate Doom's state and mobj tables.

Additionally this serves as a demonstration of a potential subset of features that any port could be reasonably expected to implement. See language documentation in the docs directory for specific details.

Project requires a C++11 compiler and has no dependencies other than the language standard libraries.